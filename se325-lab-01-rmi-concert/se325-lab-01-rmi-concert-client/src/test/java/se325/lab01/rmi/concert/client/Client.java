package se325.lab01.rmi.concert.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import se325.lab01.concert.common.*;
//import se325.lab01.rmi.concert.common.Config;
//import se325.lab01.whiteboard.common.FullException;
//import se325.lab01.whiteboard.common.Graphic;
//import se325.lab01.whiteboard.common.Shape;
//import se325.lab01.whiteboard.common.ShapeFactory;

import org.junit.BeforeClass;
import org.junit.Test;

//import se325.lab01.whiteboard.common.Config;
//import se325.lab01.whiteboard.common.ShapeFactory;

import se325.lab01.concert.common.*;


/**
 * JUnit test client for the RMI whiteboard application.
 */
public class Client {
	
    // Proxy object to represent the remote ConcertServant service.
    private static ConcertService proxy;//idk what here

    /**
     * One-time setup method to retrieve the ShapeFactory proxy from the RMI
     * Registry.
     */
    @BeforeClass
    public static void getProxy() {
        try {
            // Instantiate a proxy object for the RMI Registry, expected to be
            // running on the local machine and on a specified port.
            Registry lookupService = LocateRegistry.getRegistry("localhost", Config.REGISTRY_PORT);

            // Retrieve a proxy object representing the ShapeFactory.
            proxy = (ConcertService) lookupService.lookup(Config.SERVICE_NAME);
        } catch (RemoteException e) {
            System.out.println("Unable to connect to the RMI Registry");
        } catch (NotBoundException e) {
            System.out.println("Unable to acquire a proxy for the Concert service");
        }
    }
    

    @Test
    public void testCreate() throws RemoteException {
        try {
            // Use the ShapeFactory proxy to create a couple of remote Shape
            // instances. newShape() returns proxies for the new remote Shapes.
        	List<Concert> remoteConcerts = proxy.getAllConcerts();
        	int count = remoteConcerts.size();
            Concert concertA = proxy.createConcert(new Concert((long) 1, "A", null));
            Concert concertB = proxy.createConcert(new Concert((long) 2, "B", null));

            // Query the new Concert object's ids. the getId() calls are remote
            // method invocations on the Shapes that have been created on the
            // the server.
            System.out.println("concertA's Id is " + concertA.getId());
            System.out.println("concertB's Id is " + concertB.getId());

            // Query the remote factory.
            
            List<Concert> remoteConcertsAfter = proxy.getAllConcerts();
            assertTrue(remoteConcertsAfter.contains(concertA));
            assertTrue(remoteConcertsAfter.contains(concertB));
            assertEquals(count + 2, remoteConcertsAfter.size());

            for (Concert c : remoteConcerts) {
                // First iteration of this loop calls getAllstate() on the
                // same remote Shape object that shapeA acts as a remote
                // reference for, the second iteration on shapeB's remote
                // object.
                System.out.println(c.getId().toString());
                System.out.println(c.getTitle().toString());
            }
        } catch (Exception e) {
            fail();
        }
    }

}
