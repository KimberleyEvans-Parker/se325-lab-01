package se325.lab01.concert.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;

public class ConcertServant extends UnicastRemoteObject implements ConcertService {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// List of Concerts.
    static private Map<Long, Concert> concerts = new HashMap<>();
	
    // Unique id of the next concert to create.
    static private long nextId = 0;

	protected ConcertServant() throws RemoteException {
		super();
	}

	@Override
	public Concert createConcert(Concert concert) throws RemoteException  {
    	long id = nextId++;
    	Concert newConcert = concert;
    	newConcert.setId(id);
    	concerts.put(id, concert);
    	return newConcert;
    }

	@Override
	public Concert getConcert(Long id) throws RemoteException {
    	Concert concert = concerts.get(id); // sets it to null if there is no match
    	return concert;
    }

	@Override
	public boolean updateConcert(Concert concert) throws RemoteException {
    	// return false if...
    	if (concerts.get(concert.getId()) == null) {
    		return false;
    	}
    	concerts.put(concert.getId(), concert);
    	return true;
    }

	@Override
	public boolean deleteConcert(Long id) throws RemoteException {
    	Concert concert = concerts.remove(id);
    	if (concert == null) {
    		return false;
    	}
    	return true;
    }

	@Override
	public List<Concert> getAllConcerts() throws RemoteException {
    	List<Concert> concertList = new ArrayList<>(concerts.values());
    	return concertList;
    }

	@Override
	public void clear() throws RemoteException {
    	concerts.clear();
    }

}
